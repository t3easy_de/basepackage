===========
basepackage
===========

:Author:
	Jan Kiesewetter

Function
========
This package should ship reasonable default settings, libraries, templates, etc. to use in a TYPO3 distribution package.

Usage
=====
#. Create your distribution
#. Depend on this basepackage
#. Use, extend, override settings, library

Contribute
==========
If you would like to contribute, *just do it*:

- Write a issue
- Sent a pull request
- Share

Composer
========
Add the git repository to the list of repositories::

	"repositories": [
			{ "type": "composer", "url": "https://composer.typo3.org/" },
			{ "type": "git", "url":  "https://bitbucket.org/t3easy_de/basepackage" }
	],

Add the package name to the require part::

	"require": {
			"typo3/cms": "^7.6.0",
			"t3easy/basepackage": "dev-master"
	},

Run::

	composer update

