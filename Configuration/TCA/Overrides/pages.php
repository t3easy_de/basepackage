<?php
defined('TYPO3_MODE') or die();

call_user_func(
    function ($extKey) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/TSconfig/Page/Base.tsconfig',
            'All base settings: TCEFORM, RTE, Backend Layouts'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/TSconfig/Page/TCEFORM.tsconfig',
            'TCEFORM'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/TSconfig/Page/RTE.tsconfig',
            'RTE settings'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/TSconfig/Page/BackendLayouts.tsconfig',
            'Backend Layouts'
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/TSconfig/Page/German.tsconfig',
            'German settings (not included in base)'
        );
    },
    'basepackage'
);
