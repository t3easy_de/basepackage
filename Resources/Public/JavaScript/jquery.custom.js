// load google fonts async
WebFont.load({
    google: { families: [ 'Source+Sans+Pro:400,300,600,600italic,400italic,300italic:latin' ] }
});

$(document).ready(function() {
	var isTouch = 'click';
	if(Modernizr.touch) {
		isTouch = 'touchend'
	}

	// resposive nav init
	var navigation = responsiveNav(".navigation", {
		insert: "before",
		open: function(){
			$('.nav-collapse a').on(isTouch, function(){
				navigation.toggle();
			});
		}
	});

});
