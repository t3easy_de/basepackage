/* global require, define */
require.config({
	paths: {
		'jquery': [
			'//code.jquery.com/jquery-2.2.3.min'
		],
		'jquery.magnificPopup': [
			'//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min'
		],
		'WebFont': [
			'//cdnjs.cloudflare.com/ajax/libs/webfont/1.6.24/webfontloader'
		]
	},
	'shim': {
		'jquery.magnificPopup': {
			deps: ['jquery'],
			exports: 'jQuery.fn.magnificPopup'
		},
		'WebFont': {
			exports: 'WebFont'
		}
	}
});

// Wrap Modernizr
define('Modernizr', function() {
	'use strict';
	return window.Modernizr;
});

require(['app']);
