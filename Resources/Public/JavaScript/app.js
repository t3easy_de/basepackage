/* global define */
define(['jquery', 'jquery.magnificPopup'], function($) {
	'use strict';
	loadLightbox();

	function loadLightbox() {
		var lightbox = $('.lightbox');
		if (lightbox.length > 0) {
			lightbox.magnificPopup({
				type: 'image',
				zoom: {
					enabled: true
				},
				image: {
					titleSrc: function(item) {
						var title = item.el.attr('title');
						var description = item.el.attr('alt');
						return ((title) ? title : '') + ((description) ? '<small>' + description + '</small>' : '');
					}
				},
				gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0, 1]
				}
			});
		}
	}

});