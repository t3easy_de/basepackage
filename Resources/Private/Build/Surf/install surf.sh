#!/bin/bash

curl -sS https://getcomposer.org/installer | php

php composer.phar create-project typo3/flow-base-distribution surf
cd surf
php ../composer.phar require typo3/surf=dev-master
./flow surf:list