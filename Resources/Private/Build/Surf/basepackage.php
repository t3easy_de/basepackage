<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/** @var $deployment \TYPO3\Surf\Domain\Model\Deployment */

// The package key
$packageKey = 'basepackage';

// The git repo
$repositoryUrl = 'git@bitbucket.org:t3see/' . $packageKey . '.git';

// Where to deploy the package
$deploymentPath = '/var/www/deployment/' . $packageKey;

// Absolute path to TYPO3 root
$typo3Path = '~/typo3cms/projekt1';

// PHP, empty for default or string with space at the end
$php = 'env -i /usr/local/bin/php5-54LATEST-CLI ';

// setup the workflow
$workflow = new \TYPO3\Surf\Domain\Model\SimpleWorkflow();
$deployment->setWorkflow($workflow);

$node = new \TYPO3\Surf\Domain\Model\Node('localhost');
$node->setHostname('basepackage.t3easy.de');
//$node->setOption('username', 'myuser');
// You do not want to do that...
//$node->setOption('password', 'yourSshPasswordHere');

$application = new \TYPO3\Surf\Application\BaseApplication($packageKey);
$application->setDeploymentPath($deploymentPath);
$application->setOption('repositoryUrl', $repositoryUrl);
$application->setOption('keepReleases', 5);
$application->addNode($node);

// package local and transfer with rsync
$application->setOption('packageMethod', 'git');
$application->setOption('transferMethod', 'rsync');
$application->setOption('updateMethod', null);

// run bundler to compile sass to css after git package
$workflow->defineTask(
    't3easy.package:bundle',
    'typo3.surf:localshell',
    [
        'command' => 'cd {workspacePath} && bundle exec compass compile -e production -s compressed && rm -rf .sass-cache'
    ]
);
$workflow->afterTask('typo3.surf:package:git', 't3easy.package:bundle', $application);

// Symlink new release package
$workflow->defineTask(
    't3easy.package:symlinkrelease',
    'typo3.surf:shell',
    [
        'command' => 'cd ' . $typo3Path . '/typo3conf/ext && rm -f ' . $packageKey . ' && ln -s {releasePath} ' . $packageKey
    ]
);
$workflow->afterTask('typo3.surf:symlinkrelease', 't3easy.package:symlinkrelease', $application);

// Install the package
$workflow->defineTask(
    't3easy.package:installpackage',
    'typo3.surf:shell',
    [
        'command' => $php . $typo3Path . '/typo3/cli_dispatch.phpsh extbase extension:install ' . $packageKey
    ]
);
$workflow->afterTask('t3easy.package:symlinkpackage', 't3easy.package:installpackage', $application);

// Test frontend after deployment
$smokeTestOptions = [
    'url' => 'http://basepackage.t3easy.de',
    'remote' => true,
    'expectedStatus' => 200,
    'expectedRegexp' => '/TYPO3 Basepackage/'
];
$workflow->defineTask('t3easy:smoketest', 'typo3.surf:test:httptest', $smokeTestOptions);
$workflow->addTask('t3easy:smoketest', 'test', $application);

// Add application to this deployment
$deployment->addApplication($application);
